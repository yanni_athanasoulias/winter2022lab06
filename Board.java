public class Board {
	private die die1;
	private die die2;
	private boolean[] closedTiles;
	
	public Board() {
		this.die1 = new die();
		this.die2 = new die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString() {
		String tiles = "";
		for (int i = 0; i < this.closedTiles.length; i++) {
			if (closedTiles[i] == false) {
				tiles += i + 1 + " ";
			} else {
				tiles += "X";
			}
		}
		return tiles;
	}
	
	public boolean playATurn() {
		this.die1.roll();
		this.die2.roll();
		System.out.println(die1 + " " + die2);
		int diesum = this.die1.getPips() + this.die2.getPips();
		if (closedTiles[diesum-1] == false) {
			this.closedTiles[diesum-1] = true;
			System.out.println("Closing tile: " + diesum);
			return false;
		} else {
			System.out.println("The tile at this position" + " " + diesum + " " + "is already shut.");
			return true;
		}
	}
}
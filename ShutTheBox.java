public class ShutTheBox {
	public static void main (String[] args) {
		System.out.println("Welcome to the SHUT THE BOX game!!!!");
		Board brd = new Board();
		boolean gameOver = false;
		while (gameOver != true) {
			System.out.println("Player 1's turn:" + " " + brd);
			if (brd.playATurn() == true) {
				System.out.println("Game Over. PLayer two wins");
				gameOver = true;
			} else {
				System.out.println("Player 2's turn:" + " " + brd);
			}
			if (brd.playATurn() == true) {
				System.out.println("Game Over. PLayer one wins");
			}
		}
	}
}
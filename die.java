import java.util.Random;
public class die {
	private int pips;
	private Random rdm;
	public die() {
		this.pips = 1;
		this.rdm = new Random();
	}
	public int getPips() {
		return this.pips;
	}
	public Random getRdm() {
		return this.rdm;
	}
	public void roll() {
		this.pips = rdm.nextInt(6)+1;
	}
	public String toString() {
		return "YOUR NUMBER IS: " + this.pips;
	}
}